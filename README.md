# Table of Content

- [Table of Content](#table-of-content)
  - [Introduction ](#introduction-)
  - [SKMEI ](#skmei-)
    - [Description ](#description-)
  - [Salus ](#salus-)
    - [Description ](#description--1)
  - [QSafe ID ](#qsafe-id-)
    - [Description ](#description--2)
    - [Launching ](#launching-)

## Introduction <a name="introduction"></a>
Place for reverse engineering.

## SKMEI <a name="skmei"></a>
Devices from SKMEI. Mostly it is watches.

### Description <a name="description-skmei"></a>

## Salus <a name="salus"></a>
Devices from Salus. Mostly temperature control devices.

### Description <a name="description-salus"></a>

## QSafe ID <a name="qsafe-id"></a>
A place to reverse engineer device QSafe ID.

### Description <a name="description-qsafe-id"></a>

### Launching <a name="launching-qsafe-id"></a>
